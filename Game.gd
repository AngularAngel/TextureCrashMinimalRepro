extends Node3D

# Called when the node enters the scene tree for the first time.
func _ready():
	var textureImages = [];
	
	for i in 1:
		textureImages.append(Image.create(1, 1, false, Image.FORMAT_RGBAF))
	
	var texture =  ImageTexture3D.new()
	texture.create(Image.FORMAT_RGBAF, 1, 1, 1, false, textureImages)
	
	texture.update(textureImages)
